from nose.tools import assert_equal, assert_almost_equal
def average(vals):
    try:
    	return sum(vals)/len(vals)
    except ValueError as v:
        print(v)
    except ZeroDivisionError as v:
        print(v)

def test1():
    obs = average([0.0, 2.0])
    exp = 1.0
    assert_equal(obs, exp)
    
def test2():
    obs = average([])
    exp = 0.0
    assert_equal(obs, exp)
def test3():
    obs = average([2, 3])
    exp = 2.5
    assert_equal(obs, exp)
test1()
test2()
test3()
