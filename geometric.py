import math
#Class definition. its name is Point
class Point:
    #attributes. private attributes
    __x=0
    __y=0
    #constructor. always with this name
    def __init__(self, x, y):
        self.__x=x
        self.__y=y
    #Public Method: a setter
    def setX(self, x):
        self.__x=x
    def setY(self, y):
        self.__y=y
    #Methods: a getter
    def getX(self):
        return self.__x
    def getY(self):
        return self.__y
    def getXY(self):
        return [self.__x, self.__y]

class Line:
    __begin=Point(0,0)
    __end=Point(1,1)
    def __init__(self, begin, end):
        self.__begin=begin
        self.__end=end
    def setBegin(self, begin):
        self.__begin=begin
    def setEnd(self, end):
        self.__end=end
    def getBegin(self):
        return self.__begin
    def getEnd(self):
        return self.__end
    #def getBeginX(self):
    def getLength(self):
        return ((self.__begin.getX()-self.__end.getX())**2+(self.__begin.getY()-self.__end.getY())**2)**(1/2)

class Circle:
    #public attribute
    label="circle"
    #private attribute
    __center=Point(0,0)
    __radius=1.0
    def __init__(self, center, radius):
        self.__center=center
        self.__radius=radius
    def setCenter(self, center):
        self.__center=center
    def setRadius(self, radius):
        self.__radius=radius
    def getCenter(self):
        return self.__center
    def getRadius(self):
        return self.__radius
    #def getBeginX(self):
    def getArea(self):
        return math.pi*self.__radius**2
    def getCircumference(self):
        return 2*math.pi*self.__radius

class Cylinder(Circle):
    __height=2.0
    def __init__(self, height):
        self.__height=height
        self.label="cylinder"
    def setHeight(self, height):
        self.__height=height
    def getHeight(self):
        return self.__height
    def getVolume(self):
        return self.getArea()*self.getHeight()
    def getSurfaceArea(self):
        return 2*math.pi*self.getRadius()*self.getHeight()+2*math.pi*self.getRadius()**2
